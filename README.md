# instapic

Create images appended with borders to match instagram's picture size ratio limitations.

## Use

`./instapic -i input.jpg -o output.jpg`

| Flag | Default | Description |
| --- | --- | --- |
| `-i` | N/A | path of input JPG image |
| `-o` | N/A | path of output JPG image |
| `-v` | false | verbose output flag |
