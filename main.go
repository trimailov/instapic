package main

import (
	"flag"
	"fmt"
	"image"
	"image/draw"
	"image/jpeg"
	"log"
	"os"
)

var (
	inputPath  string
	outputPath string
	verbose    bool
	square     bool
)

const (
    horizontalRatioLimit = 1.91
    verticalRatioLimit = 0.8
    squareRatio = 1
)

func Log(format string, a ...interface{}) {
	if verbose {
		fmt.Printf(format, a...)
	}
}

func init() {
	flag.StringVar(&inputPath, "i", "", "Path of the input JPG image")
	flag.StringVar(&outputPath, "o", "output.jpg", "Path of the output JPG image")
	flag.BoolVar(&verbose, "v", false, "Verbose output flag")
	flag.BoolVar(&square, "square", false, "Append white borders to make image square format")
}

func main() {
	flag.Parse()

	if inputPath == "" {
		log.Fatal("Input flag `-i` must be non empty")
	}

	jpegImage := loadJpegImage(inputPath)

	if square == true {
		paddedImage := createSquarePaddedImage(jpegImage)
		if paddedImage != nil {
			saveJpegImage(paddedImage)
		} else {
			log.Fatal("Could not create image")
		}
	} else {
		paddedImage := createAutoPaddedImage(jpegImage)
		if paddedImage != nil {
			saveJpegImage(paddedImage)
		} else {
			log.Fatal("Could not create image")
		}
	}
}

func loadJpegImage(fileName string) image.Image {
	file, fileOpenError := os.Open(fileName)
	defer file.Close()
	if fileOpenError != nil {
		log.Fatal(fileOpenError)
	} else {
		Log("Read file: %v\n", fileName)
	}

	jpegImage, jpegDecodeError := jpeg.Decode(file)
	if jpegDecodeError != nil {
		log.Fatal(jpegDecodeError)
	} else {
		Log("Decoded file as JPG\n")
	}

	return jpegImage
}

func createSquarePaddedImage(jpegImage image.Image) image.Image {
	pictureWidth := jpegImage.Bounds().Max.X
	pictureHeight := jpegImage.Bounds().Max.Y

	pictureStartLine := (pictureWidth - pictureHeight) / 2

	paddedImageRectangle := image.Rect(0, 0, pictureWidth, pictureWidth)
	paddedImage := image.NewNRGBA(paddedImageRectangle)

	draw.Draw(paddedImage, paddedImage.Bounds(), image.White, image.Point{0, 0}, draw.Src)
	draw.Draw(paddedImage, paddedImage.Bounds(), jpegImage, image.Point{0, -pictureStartLine}, draw.Src)

	return paddedImage
}

func createAutoPaddedImage(jpegImage image.Image) image.Image {
	pictureWidth := jpegImage.Bounds().Max.X
	pictureHeight := jpegImage.Bounds().Max.Y

	ratio := float64(pictureWidth) / float64(pictureHeight)

	if ratio > horizontalRatioLimit {
		Log("Image is horizontal\n")
		Log("Ratio is >1.91\n")
		paddedImageWidth := pictureWidth
		paddedImageHeight := int(float64(pictureWidth) / 1.91)
		paddedImageRectangle := image.Rect(0, 0, paddedImageWidth, paddedImageHeight)
		paddedImage := image.NewNRGBA(paddedImageRectangle)
		pictureStartLine := (paddedImageHeight - pictureHeight) / 2

		draw.Draw(paddedImage, paddedImage.Bounds(), image.White, image.Point{0, 0}, draw.Src)
		draw.Draw(paddedImage, paddedImage.Bounds(), jpegImage, image.Point{0, -pictureStartLine}, draw.Src)
		return paddedImage
	} else if ratio < verticalRatioLimit {
		Log("Image is vertical\n")
		Log("Ratio is <0.8\n")
		paddedImageHeight := pictureHeight
		paddedImageWidth := int(float64(pictureHeight) * 0.8)
		paddedImageRectangle := image.Rect(0, 0, paddedImageWidth, paddedImageHeight)
		paddedImage := image.NewNRGBA(paddedImageRectangle)
		pictureStartLine := (paddedImageWidth - pictureWidth) / 2

		draw.Draw(paddedImage, paddedImage.Bounds(), image.White, image.Point{0, 0}, draw.Src)
		draw.Draw(paddedImage, paddedImage.Bounds(), jpegImage, image.Point{-pictureStartLine, 0}, draw.Src)
		return paddedImage
	} else {
		fmt.Printf("Ratio is in bounds - did not create any output image\n")
        return nil
    }
}

func saveJpegImage(jpgImage image.Image) {
	toImg, err := os.Create(outputPath)
	defer toImg.Close()
	if err != nil {
		log.Fatal(err)
	} else {
		Log("Created output file at: %v\n", outputPath)
	}
	jpeg.Encode(toImg, jpgImage, &jpeg.Options{Quality: jpeg.DefaultQuality})
	Log("Encoded image as JPG: %v\n", outputPath)
}
